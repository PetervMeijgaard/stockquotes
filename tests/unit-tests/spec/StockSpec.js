/*global beforeEach, afterEach */
/*global describe, it, expect */
/*global window, eb, loadFixtures */
/*global app */
/*jslint browser: true*/

describe('Connectivity', function () {
    'use strict';
    it('should be connected to localhost AJAX', function () {
        expect(app.settings.ajax.url).toBe('http://localhost:8082');
    });

    it('should be connected to localhost Socket', function () {
        expect(app.settings.socket.url).toBe('http://localhost:1337');
    });
});

describe('Html', function () {
    'use strict';

    beforeEach(function () {
        app.init();
    });

    afterEach(function () {
        document.body.removeChild(document.querySelector("#container"));
    });

    it('should have an h1-tag with the correct content', function () {
        var expected = 'Real Time Stockquotes App',
            actual = document.querySelector('h1').innerText;

        expect(actual).toBe(expected);
    });

    it('should have 25 rows', function () {
        var actual = document.querySelector('#table_content').querySelectorAll('tr').length;
        expect(actual).toBe(25);
    });
});

describe('checkType', function () {
    'use strict';
    it('should return static if none is given', function () {
        var expected = 'static',
            actual = app.checkType();
        expect(actual).toBe(expected);
    });

    it('should return the given type', function () {
        var expected = 'random',
            actual = app.checkType(expected);

        expect(actual).toBe(expected);
    });
});

describe('checkRefreshRate', function () {
    'use strict';

    it('should not be lower than the minimum', function () {
        var expected = app.settings.minRefreshRate,
            actual = app.checkRefreshRate(10);

        expect(actual).toBe(expected);
    });

    it('should return the given refresh rate if it is above the minimum', function () {
        var expected = 1000,
            actual = app.checkRefreshRate(1000);

        expect(actual).toBe(expected);
    });
});

describe('updateTable', function () {
    'use strict';

    beforeEach(function () {
        app.initHtml();
        app.initTable();
    });

    afterEach(function () {
        document.body.removeChild(document.querySelector("#container"));
    });

    it('should insert the data', function () {
        var data = [{
                col0: 'BCS',
                col1: '18.99',
                col2: '12/12/2015',
                col3: '01:34',
                col4: '1.57',
                col5: 'N/A',
                col6: 'N/A',
                col7: 'N/A',
                col8: '8900'
            }],
            expected = data,
            body = document.querySelector('#table_content'),
            actual = [],
            i,
            y,
            columns;

        app.updateTable(data);

        for (i = 0; i < body.rows.length; i = i + 1) {
            columns = {};
            for (y = 0; y < body.rows[i].getElementsByTagName('td').length; y += 1) {
                if (y === 4) {
                    columns['col' + y] = parseFloat(body.rows[i].getElementsByTagName('td')[y].innerHTML);
                } else {
                    columns['col' + y] = body.rows[i].getElementsByTagName('td')[y].innerHTML;
                }
            }
            actual.push(columns);
        }


        expect(actual[0].col0).toBe(expected[0].col0);
        expect(actual[0].col1).toBe(expected[0].col1);
        expect(actual[0].col2).toBe(expected[0].col2);
        expect(actual[0].col3).toBe(expected[0].col3);
        expect(actual[0].col4).toBe(expected[0].col4);
        expect(actual[0].col5).toBe(expected[0].col5);
        expect(actual[0].col6).toBe(expected[0].col6);
        expect(actual[0].col7).toBe(expected[0].col7);
        expect(actual[0].col8).toBe(expected[0].col8);
    });

    it('should assign the loser-class', function () {
        var data = [{
                col0: 'BCS',
                col1: '18.99',
                col2: '12/12/2015',
                col3: '01:34',
                col4: '-1',
                col5: 'N/A',
                col6: 'N/A',
                col7: 'N/A',
                col8: '8900'
            }],
            expected = 'loser',
            body = document.querySelector('#table_content'),
            actual;

        app.updateTable(data);
        actual = body.rows[0].getElementsByTagName('td')[4].className;

        expect(actual).toBe(expected);
    });

    it('should assign the winner-class', function () {
        var data = [{
                col0: 'BCS',
                col1: '18.99',
                col2: '12/12/2015',
                col3: '01:34',
                col4: '1',
                col5: 'N/A',
                col6: 'N/A',
                col7: 'N/A',
                col8: '8900'
            }],
            expected = 'winner',
            body = document.querySelector('#table_content'),
            actual;

        app.updateTable(data);
        actual = body.rows[0].getElementsByTagName('td')[4].className;

        expect(actual).toBe(expected);
    });

    it('should assign the no-change-class', function () {
        var data = [{
                col0: 'BCS',
                col1: '18.99',
                col2: '12/12/2015',
                col3: '01:34',
                col4: '0',
                col5: 'N/A',
                col6: 'N/A',
                col7: 'N/A',
                col8: '8900'
            }],
            expected = 'no-change',
            body = document.querySelector('#table_content'),
            actual;

        app.updateTable(data);
        actual = body.rows[0].getElementsByTagName('td')[4].className;

        expect(actual).toBe(expected);
    });
});