/*jslint browser: true*/
/*global io, console, staticData */
(function () {
    'use strict';
    var app;
    app = window.app = {
        /**
         * The settings for the app
         */
        settings: {
            minRefreshRate: 500,
            type: 'static',
            setType: function () {
                var type = location.search.split('type=')[1];
                if (type) {
                    type = type.split('&')[0];
                }

                this.type = app.checkType(type);
            },
            refreshRate: 500,
            setRefreshRate: function () {
                var refreshRate = location.search.split('refresh_rate=')[1];
                if (refreshRate) {
                    refreshRate = refreshRate.split('&')[0];
                }
                refreshRate = parseInt(refreshRate, 10) || app.settings.minRefreshRate;

                this.refreshRate = app.checkRefreshRate(refreshRate);
            },
            socket: {
                url: 'http://localhost:1337'
            },
            ajax: {
                url: 'http://localhost:8082'
            }
        },

        /**
         * Function used to check the type
         * @param {string} type The given type
         * @returns {string} The type
         */
        checkType: function (type) {
            type = type || 'static';
            return type;
        },

        /**
         * Function used to check and set the refresh rate
         * @param {int} refreshRate The given refresh rate
         * @returns {int} The refresh rate
         */
        checkRefreshRate: function (refreshRate) {
            // Checks the refresh rate with the minimum refresh rate.
            // This prevents the server from overloading
            if (refreshRate < app.settings.minRefreshRate) {
                refreshRate = app.settings.minRefreshRate;
            }
            return refreshRate;
        },

        /**
         * The used socket for this application
         * @returns {function} The socket instance
         */
        socket: function () {
            return io(this.settings.socket.url);
        },

        /**
         * Function used to update the table
         * @param {Array} data The given data
         */
        updateTable: function (data) {
            var tableBody = document.getElementById('table_content'),
                tableRow,
                tableCell;

            tableBody.innerHTML = '';

            data.forEach(function (quote) {
                var item;
                tableRow = document.createElement('tr');

                for (item in quote) {
                    if (quote.hasOwnProperty(item)) {
                        tableCell = document.createElement('td');
                        if (item === 'col4') {
                            quote[item] = parseFloat(quote[item]);
                            if (quote[item] > 0) {
                                tableCell.className = 'winner';
                            } else if (quote[item] < 0) {
                                tableCell.className = 'loser';
                            } else {
                                tableCell.className = 'no-change';
                            }
                        }
                        tableCell.innerText = quote[item];

                        tableRow.appendChild(tableCell);
                    }
                }

                tableBody.appendChild(tableRow);
            });
        },

        /**
         * Function used to parse the data
         * @param {Array} data The given array of data
         */
        parseData: function (data) {
            app.updateTable(data);
        },

        /**
         * Function used to parse the AJAX request
         * @param {object} request The given request
         */
        parseAjaxRequest: function (request) {
            var data;
            if (request.readyState === 4 && request.status === 200) {
                data = JSON.parse(request.responseText);
                app.parseData(data.query.results.row);
            }
        },

        /**
         * Function used to fetch the data using the AJAX source
         */
        fetchAjax: function () {
            var request = new XMLHttpRequest();
            request.open('GET', app.settings.ajax.url, true);
            request.onreadystatechange = function () {
                app.parseAjaxRequest(request);
            };
            request.send();
        },

        /**
         * Function used to fetch the data using the Socket source
         */
        fetchSocket: function () {
            app.socket().on('stockquotes', function (data) {
                app.parseData(data.query.results.row);
            });
        },

        /**
         * Function to fetch the static data
         */
        fetchStaticData: function () {
            app.parseData(staticData.query.results.row);
        },

        /**
         * Function to generate random data
         */
        fetchRandomData: function () {
            var data = [],
                arrayLength = 25,
                minDate = new Date(2014, 0, 1),
                maxDate = new Date(),
                i,
                companies = [
                    'BCS',
                    'STT',
                    'JPM',
                    'LGEN.L',
                    'UBS',
                    'DB',
                    'BEN',
                    'CS',
                    'BK',
                    'KN.PA',
                    'GS',
                    'LM',
                    'MS',
                    'MTU',
                    'NTRS',
                    'GLE.PA',
                    'BAC',
                    'AV',
                    'SDR.L',
                    'DODGX',
                    'SLF',
                    'SL.L',
                    'NMR',
                    'ING',
                    'BNP.PA'
                ],
                company;

            for (i = 1; i < arrayLength; i += 1) {
                if (companies.length <= i) {
                    company = 'Company ' + i;
                } else {
                    company = companies[i - 1];
                }
                data.push({
                    col0: company,
                    col1: Math.floor(Math.random() * 1000) + 1,
                    col2: new Date(minDate.getTime() + Math.random() * (maxDate.getTime() - minDate.getTime())).toDateString(),
                    col3: new Date(minDate.getTime() + Math.random() * (maxDate.getTime() - minDate.getTime())).toLocaleTimeString(),
                    col4: Math.round((i - 50) * Math.floor(Math.random() * 2) % 10).toString(),
                    col5: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col6: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col7: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col8: (Math.floor(Math.random() * 1000) + 1).toString()
                });
            }
            app.parseData(data);
        },

        /**
         * Function used to fetch the data.
         * It als determines which source it will use.
         */
        fetchData: function () {
            switch (app.settings.type) {
            case 'ajax':
                app.fetchAjax();
                break;
            case 'socket':
                app.fetchSocket();
                break;
            case 'static':
                app.fetchStaticData();
                break;
                // Fallback
            case 'random':
                app.fetchRandomData();
                break;
            default:
                app.fetchStaticData();
                break;
            }

            setTimeout(app.fetchData, app.settings.refreshRate);
        },

        /**
         * Function to initialize the table
         */
        initTable: function () {
            var container = document.getElementById('container'),
                table = document.createElement('table'),
                tableHead = document.createElement('thead'),
                tableHeadCel,
                tableBody = document.createElement('tbody'),
                tableRow = document.createElement('tr'),
                header = [
                    'Company',
                    'Stocks',
                    'Date',
                    'Time',
                    'Change',
                    'Avg',
                    'High',
                    'Low',
                    'Volume'
                ];

            header.forEach(function (header) {
                tableHeadCel = document.createElement('th');
                tableHeadCel.innerText = header;
                tableRow.appendChild(tableHeadCel);
            });

            tableBody.id = 'table_content';
            tableHead.appendChild(tableRow);

            table.className = 'table table-hover';
            table.appendChild(tableHead);
            table.appendChild(tableBody);

            container.appendChild(table);
        },

        /**
         * Function to initialize the html
         */
        initHtml: function () {
            var container = document.createElement('div'),
                body = document.body,
                header = document.createElement('h1');

            header.innerText = 'Real Time Stockquotes App';

            container.className = 'container';
            container.id = 'container';
            container.appendChild(header);
            body.appendChild(container);
        },

        /**
         * Function used to initialize the stock quotes application
         */
        init: function () {
            app.settings.setType();
            app.settings.setRefreshRate();

            app.initHtml();
            app.initTable();
            app.fetchData();
        }
    };
}());