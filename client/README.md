Document all concepts and your implementation decisions.

---


## Return values from yahoo ##
Values available in the test urls.

    s   Symbol
    l1  Last Trade (Price Only)
    d1  Date of Last Trade
    t1  Time of Last Trade
    c1  Change (in points)
    o   Open price
    h   Dayâ€™s High
    g   Dayâ€™s Low
    v   Volume

All return values

    a	Ask
    a2	Average Daily Volume
    a5	Ask Size
    b	Bid
    b2	Ask (Real-time)
    b3	Bid (Real-time)
    b4	Book Value
    b6	Bid Size
    c	Change and Percent Change
    c1	Change
    c3	Commission
    c6	Change (Real-time)
    c8	After Hours Change (Real-time)
    d	Dividend/Share
    d1	Last Trade Date
    d2	Trade Date
    e	Earnings/Share
    e1	Error Indication (returned for symbol changed / invalid)
    e7	EPS Est. Current Yr
    e8	EPS Estimate Next Year
    e9	EPS Est. Next Quarter
    f6	Float Shares
    g	Dayâ€™s Low
    g1	Holdings Gain Percent
    g3	Annualized Gain
    g4	Holdings Gain
    g5	Holdings Gain Percent (Real-time)
    g6	Holdings Gain (Real-time)
    h	Dayâ€™s High
    i	More Info
    i5	Order Book (Real-time)
    j	52-week Low
    j1	Market Capitalization
    j3	Market Cap (Real-time)
    j4	EBITDA
    j5	Change From 52-week Low
    j6	Percent Change From 52-week Low
    k	52-week High
    k1	Last Trade (Real-time) With Time
    k2	Change Percent (Real-time)
    k3	Last Trade Size
    k4	Change From 52-wk High
    k5	Percent Change From 52-week High
    l	Last Trade (With Time)
    l1	Last Trade (Price Only)
    l2	High Limit
    l2	High Limit
    l3	Low Limit
    l3	Low Limit
    m	Day's Range
    m2	Dayâ€™s Range (Real-time)
    m3	50-day Moving Avg
    m4	200-day Moving Average
    m5	Change From 200-day Moving Avg
    m6	Percent Change From 200-day Moving Average
    m7	Change From 50-day Moving Avg
    m8	Percent Change From 50-day Moving Average
    n	Name
    n4	Notes
    o	Open
    p	Previous Close
    p1	Price Paid
    p2	Change in Percent
    p5	Price/Sales
    p6	Price/Book
    q	Ex-Dividend Date
    q	Ex-Dividend Date
    r	P/E Ratio
    r1	Dividend Pay Date
    r2	P/E (Real-time)
    r5	PEG Ratio
    r6	Price/EPS Est. Current Yr
    r7	Price/EPS Estimate Next Year
    s	Symbol
    s1	Shares Owned
    s7	Short Ratio
    s7	Short Ratio
    t1	Last Trade Time
    t6	Trade Links
    t7	Ticker Trend
    t8	1 yr Target Price
    v	Volume
    v1	Holdings Value
    v7	Holdings Value (Real-time)
    v7	Holdings Value (Real-time)
    w	52-week Range
    w	52-week Range
    w1	Day's Value Change
    w1	Dayâ€™s Value Change
    w4	Day's Value Change (Real-time)
    w4	Dayâ€™s Value Change (Real-time)
    x	Stock Exchange
    x	Stock Exchange
    y	Dividend Yield
    y	Dividend Yield

### Get the company name with a ticker symbol
    http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=TICKER_SYMBOL&callback=YAHOO.Finance.SymbolSuggest.ssCallback

Replace `TICKER_SYMBOL` with your ticket symbol.


## Flow of the program ##
TODO: Improve this flow

 1. Initialize the application
 2. Retrieve the data
 3. Write the data to the screen
 4. Wait 5 seconds
 5. Retrieve the data

## Concepts ##
For every concept the following items:

- Short description
- Code example
- Reference to mandatory documentation
- Reference to alternative documentation (authoritative and authentic)

### Objects, including object creation and inheritance ###
#### Description ###
Object Oriented Programming is a way of programming which creates Objects.
These objects can contain properties or procedures using methods.
An proper example of an object is a rectangle.
It can have the following properties:

 - The width
 - The height
 - The color

It can have the following methods:

 - Get the width
 - Get the height
 - Get the color
 - Calculate the surface

#### Code Example ####
Let's grab our rectangle example and work it out:

    function Rectangle(width, height, color) {
        this.width = width;
        this.height = height;
        this.color = color;

        this.getWidth = function () {
            return this.width;
        };

        this.getHeight = function () {
            return this.height;
        };

        this.getColor = function () {
            return this.color;
        };

        this.calculateSurface = function () {
            return this.width * this.height;
        };
    }

    var rectangle = new Rectangle(200, 400, '#ff00aa');

Let's calculate the surface of our rectangle:

    rectangle.calculateSurface(); // returns 80000

It is possible to change a value of the object.
This can be done like so:

    rectangle.width = 400;
    rectangle.height = 100;
    rectangle.calculateSurface(); // returns 40000

Another way of creating an object in JavaScript is like so:

    var rectangle = {
        width : 200,

        height : 400,

        color : '#ff00aa',

        getWidth : function () {
            return this.width;
        },

        getHeight : function () {
            return this.height;
        },

        getColor : function () {
            return this.color;
        };
    }

Using JavaScript it you can use the `prototype` keyword which will bind functions and properties to an existing prototype.
Let's pick our previous example:

    function Rectangle(width, height, color) {
        this.width = width;
        this.height = height;
        this.color = color;

        this.getWidth = function () {
            return this.width;
        };

        this.getHeight = function () {
            return this.height;
        };

        this.calculateSurface = function () {
            return this.color
        };

        // this.calculateSurface is left out on purpose
    }

Now we can use prototype to add `calculateSurface`.

    Rectangle.prototype.calculateSurface = function () {
        return this.width * this.height;
    }

Now we can make a new instance of Rectangle and use the function `calculateSurface()` like so:

    var rect = new Rectangle(100, 200, '#FF00AA');
    rect.calculateSurface(); // 20000
#### Documentation ####
[Mozilla - Introduction to Object-Oriented JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript)
#### Alternative Documentation ####
[Wikipedia - JavaScript](https://en.wikipedia.org/wiki/JavaScript#Prototype-based_object-oriented_programming)

### WebSockets ###
#### Description ###
WebSockets is a full-duplex communication channel between the client and the server.
This way the client can communicate with the server without having to submit a form.
#### Code example ####
    var webSocketUri = 'wss://echo.websocket.org/',
        webSocket = new WebSocket(webSocketUri);

    function initWebSocket() {
        webSocket.onopen = function(e) {
            onOpen(e);
        };
        webSocket.onclose = function(e) {
            onClose(e);
        };
        webSocket.onmessage = function(e) {
            onMessage(e);
        };
        webSocket.onerror = function(e) {
            onError(e);
        };
    }

    function onOpen(e) {
        console.log('Connected to WebSocket');
    }

    function onClose(e) {
        console.log('Disconnected from WebSocket');
    }

    function onMessage(e) {
        console.log('Got a response: ' + e.data);
        webSocket.close();
    }

    function onError(e) {
        console.log('Got an error: ' + e.data);
    }

    window.addEventListener('load', initWebSocket, false);
#### Documentation ####
[Mozilla - WebSockets](https://developer.mozilla.org/nl/docs/WebSockets)
#### Alternative Documentation ####
[Wikipedia - WebSocket](https://en.wikipedia.org/wiki/WebSocket)

### XMLHttpRequest ###
#### Description ###
This object is used to send and receive data with the server.
Using this method, it is possible to manipulate the DOM without having to refresh the page.
#### Code example ####
     function sendRequest(type, url, data) {
        var request = new XMLHttpRequest();
        if(!request.withCredentials) {
            request.open(type, url, true);
            request.send(data);
        } else {
            request = null;
        }

        return request;
    }

    var req = sendRequest('GET', 'https://google.com', null);
    req.onreadystatechange = function() {
        if(req.readyState === 4 && req.status === 200) {
            console.log('Status: ' + req.status + ', response: ' + req.responseText);
        }
    };
#### Documentation ####
[Mozilla - XMLHttpRequest](https://developer.mozilla.org/nl/docs/Web/API/XMLHttpRequest)
#### Alternative Documentation ####
[Wikipedia - XMLHttpRequest](https://en.wikipedia.org/wiki/XMLHttpRequest)

### AJAX ###
#### Description ###
Just like XMLHttpRequest, AJAX is also a way to retrieve data without having to refresh the page.
AJAX stands for Asynchronous JavaScript and XML.
By using AJAX, you can exchange data with the server.
You can now update parts of the website and you do not need to reload the page.
The syntax is the same as discussed in the previous paragraph.
Using AJAX supports the following HTTP-requests:

 - GET   : This retrieves a resource from the server
 - POST  : This sends a resource on the server
 - PUT   : This updates a resource on the server
 - PATCH : This patches a resource on the server

#### Code example ####
     function sendRequest(type, url, data) {
        var request = new XMLHttpRequest();
        request.open(type, url, true);
        request.send(data);
    }

    var getRequest = sendRequest('GET', 'api/v1/user/1', null);
    getRequest.onreadystatechange = function() {
        if(getRequest.readyState === 4 && getRequest.status === 200) {
            console.log('Status: ' + getRequest.status + ', response: ' + getRequest.responseText);
        }
    };

    var postRequest = sendRequest('POST', 'api/v1/user', {
        "username" : "Testing123",
        "password" : "Test@123"
    });
    getRequest.onreadystatechange = function() {
        if(getRequest.readyState === 4 && ( getRequest.status === 200 || postRequest.status === 201)) {
            console.log('Status: ' + getRequest.status + ', response: ' + getRequest.responseText);
        }
    };
#### Documentation ####
[Mozilla - Ajax](https://developer.mozilla.org/en-US/docs/AJAX/Getting_Started)
#### Alternative Documentation ####
[Wikipedia - Ajax (programming)](https://en.wikipedia.org/wiki/Ajax_programming)

### Callbacks ###
#### Description ###
Callbacks are functions that are executed after a function has been completed.
A common example is a callback after an AJAX-request.
This is an asynchronous request and by using a callback, it is possible to process the returned data.
#### Code example ####
    function getUser(id, func, errFunc) {
        var request = new XMLHttpRequest();
        request.open('GET', 'api/v1/user' + id, true);
        request.send();
        request.onreadystatechane = function () {
            if(request.readyState === 4 && request.status === 200) {
                func(request.responseText);
            } else {
                errFunc(request.responseText);
            }
        }
    }

    // It is possible to do this inline
    getUser(1, function(data) {
        console.log(data);
    }, function(errorData) {
        console.log(errorData);
    });

    // Or it can been done by referring to a different function
    getUser(2, successFunction, errorFunction);

    function successFunction(data) {
        console.log('Success! ' + data);
    }

    function errorFunction(data) {
        console.log('There were some errors... ' + data);
    }
#### Documentation ####
[Mozilla - Declaring and Using Callbacks](https://developer.mozilla.org/en-US/docs/Mozilla/js-ctypes/Using_js-ctypes/Declaring_and_Using_Callbacks)
#### Alternative Documentation ####
[Wikipedia - Callbakc (computer programming)](https://en.wikipedia.org/wiki/Callback_computer_programming)

### How to write testable code for unit-tests ###
#### Description ###
There are a couple of things to consider when you are writing testable code:

 - Each method should have a single work
 - Functions should have return values
 - Refactor untestable code to be testable
 - The code should be loosely coupled. That way classes can be tested easily
 - Functions should only return one type
 - Only use dependency injection when it it appropriate

#### Code example ####
##### Bad example #####
    function UserController() {
        this.user = {
            username : null,
            password : null
        };

        this.sendUser = function() {
            if(!this.user.username) {
                return 'The username cannot be blank';
            }

            if(!this.user.password) {
                return 'The password cannot be blank';
            }

            if(this.user.password.length < 5) {
                return 'The password must be more than 5 characters long';
            }

            var request = new XMLHttpRequest();
            request.open('POST', 'api/v1/user', true);
            request.send(this.user);
            request.onreadystatechane = function () {
                if(request.readyState === 4 && (request.status === 200 || request.status === 201)) {
                    console.log('The user has been created');
                } else {
                    console.log('There was an error...');
                }
            }
        };
    }
##### Good example #####
    // UserModel.js
    function User(username, password) {
        this.username = username;
        this.password = password;
    }

    // UserValidation.js
    function UserValidation() {
        this.validate = function (user) {
            var validation = {
                isValid: true,
                messages: []
            };

            if (!user.username) {
                validation.isValid = false;
                messages.push('The username cannot be blank');
            }

            if (!user.password) {
                validation.isValid = false;
                messages.push('The password cannot be blank');
            }

            if (user.password.length < 5) {
                validation.isValid = false;
                messages.push('The password must be more than 5 characters long');
            }

            return validation;
        };
    }


    // UserService.js
    function UserService() {
        this.storeUser = function (data, func, errorFunc) {
            var request = new XMLHttpRequest();
            request.open('POST', 'api/v1/user', true);
            request.send(data);
            request.onreadystatechane = function () {
                if (request.readyState === 4 && (request.status === 200 || request.status === 201)) {
                    func(request.responseText);
                } else {
                    errorFunc(request.responseText);
                }
            }
        };
    }

    // RequestHandling.js
    function RequestHandling() {
        this.successHandling = function (data) {
            console.log('Success! ' + data);
        };

        this.errorHandling = function (data) {
            console.log('Failed! ' + data);
        };
    }

    // UserController.js
    function UserController(user) {
        this.user = user;
        this.validation = new UserValidation();
        this.service = new UserService();
        this.requestHandler = new RequestHandling();

        this.sendUser = function () {
            var validation = this.validation.validate(this.user);

            if (!validation.isValid) {
                this.requestHandler.errorHandling(validation.messages);
            } else {
                this.service.storeUser(this.user, this.requestHandler.errorHandling, this.requestHandler.successHandling);
            }
        };
    }

    // main.js
    var user = new User('Test123', 'Test@123');
    var userController = new UserController(user);

    userController.sendUser();
#### Documentation ####
[Jasmine - Introduction](http://jasmine.github.io/edge/introduction.html)
#### Alternative Documentation ####
[Wikipedia - Unit Testing](https://en.wikipedia.org/wiki/Unit_testing)
